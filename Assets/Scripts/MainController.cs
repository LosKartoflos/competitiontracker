﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI timerText;

    [SerializeField]
    float time = 30;

    [SerializeField]
    AudioSource audioBeep;

    [SerializeField]
    AudioSource audioBeep2;

    [SerializeField]
    AudioSource hornBeep;

    [SerializeField]
    AudioClip beepclip;


    [SerializeField]
    AudioClip hornClip;

    [SerializeField]
    TextMeshProUGUI buttonText;

    [SerializeField]
    public GameObject button1;
    public GameObject button2;

    float timer;

    bool isRunning = false;

    bool countdownStarted = false;

    bool beep1 = false, beep2 = false, beep3 = false, beep4 = false, beep5 = false;

    IEnumerator CountdownCoroutine;

    private void Start()
    {
        timerText.text = time.ToString("0") ;
        buttonText.text = "Start";
    }


    private void StartCountdown()
    {
        if (CountdownCoroutine != null)
            StopCountdown();

        CountdownCoroutine = Countdown();
        StartCoroutine(CountdownCoroutine);
    }

    private void StopCountdown()
    {
        if (CountdownCoroutine != null)
        {
            StopCoroutine(CountdownCoroutine);
            CountdownCoroutine = null;
            isRunning = false;
        }
    }

    public void toggleCountdown()
    {
        if (countdownStarted)
        {
            StopCountdown();
            timerText.text = time.ToString("0");
            Debug.Log("stop");
            buttonText.text = "Start";
        }

        else
        {
            Debug.Log("start");
            StartCountdown();
            buttonText.text = "Stop";
            
        }
            

        countdownStarted = !countdownStarted;

        button1.SetActive(!countdownStarted);
        button2.SetActive(!countdownStarted);
    }

    public void changeTime(float amount)
    {
        time += amount;

        timerText.text = time.ToString();
    }

    private IEnumerator Countdown()
    {
        timer = time;
        beep1 = false;
        beep2 = false;
        beep3 = false;
        beep4 = false;
        beep5 = false;

        isRunning = true;
        while (isRunning)
        {
            float ceiledTime = Mathf.Ceil(timer);

            switch(ceiledTime)
            {
                case 5:
                    if (!beep5)
                    {
                        beep5 = true;
                        audioBeep.PlayOneShot(beepclip);
                    }

                    break;
                case 4:
                    if (!beep4)
                    {
                        beep4 = true;
                        audioBeep2.PlayOneShot(beepclip);
                    }
                    break;
                case 3:
                    if (!beep3)
                    {
                        beep3 = true;
                        audioBeep.PlayOneShot(beepclip);
                    }
                    break;
                case 2:
                    if (!beep2)
                    {
                        beep2 = true;
                        audioBeep2.PlayOneShot(beepclip);
                    }
                    break;
                case 1:
                    if (!beep1)
                    {
                        beep1 = true;
                        audioBeep.PlayOneShot(beepclip);
                    }
                    break;
                case 0:
                    timer = time;
                    hornBeep.PlayOneShot(hornClip);

                    beep1 = false;
                    beep2 = false;
                    beep3 = false;
                    beep4 = false;
                    beep5 = false;
                    break;

                    
            }




            timerText.text = ceiledTime.ToString("0");
            timer -= Time.deltaTime;
            Debug.Log(ceiledTime);
            
            yield return new WaitForEndOfFrame();
        }

        isRunning = false;
        yield return null;
    }
}
